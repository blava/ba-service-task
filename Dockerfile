FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libicu-dev \
    libpq-dev \
    libzip-dev \
    unzip



RUN #apt-get install -y php8.1-cli php8.1-common php8.1-mysql php8.1-zip php8.1-gd php8.1-mbstring php8.1-curl php8.1-xml php8.1-bcmath
RUN #apt-get install -y php8.1-curl php8.1-zip php8.1-simplexml
RUN apt-get install -y autoconf g++ make  libxml2-dev libmcrypt-dev git libpng-dev
RUN apt-get install -y mysql-server mysql-client php php-mysql php-symfony
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY my.cnf /etc/mysql/my.cnf
RUN service mysql start \
    && mysql -u root -e "CREATE DATABASE bafood_user;"

#COPY symfony.conf /etc/apache2/sites-available/symfony.conf
RUN #a2ensite symfony.conf
RUN #a2enmod rewrite

WORKDIR /app
COPY . .

RUN composer install --no-interaction --no-scripts --no-dev --optimize-autoloader

RUN mkdir var/cache /var/log

#rm -rf /var/lib/apt/lists/*
VOLUME var
EXPOSE 8000

CMD ["symfony", "server:start", "--port 8000"]

