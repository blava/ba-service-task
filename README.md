# baf-service-user



## Installation instructions

- For proper functioning, needs database "bafood_task"
- Symfony server needs to be on port 9000
- URI for updateing the task status  is http://localhost:9000/task  with PATCH method and status parameter
-Uses rabbit-mq for message exchange


## Work Details

After creating user on the server http://localhost:8000 (Server A), message is being transferred to the server http://localhost:9000 (Server B), where server receives message and creates the task with specified userId and sets status to new.
After updating the status , server B sends message to the server A.

Server A check user(if it's exist and active), after checks the status, if it's equals to 'complete' , increases completed_tasks count for specified user
