<?php

namespace App\Service;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class GuzzleService
{
    const URI = "http://localhost:8000/user/";

    private $guzzle;
    public function __construct()
    {
        $this->guzzle = new Client();
    }

    public function getUser(int $userId) : Response
    {
        $res = $this->guzzle->request(GuzzleService::URI . $userId );
        if ($this->checkFor404($res))
            return new Response("Can't find user with ID  ". $userId, 404);
        return new Response($res->getBody(), 200);
    }

    public function updateUserStatus(int $userId, string $status)
    {

    }

    private function checkFor404($res) :bool {
        return $res->getStatusCode() == 404;
    }
}