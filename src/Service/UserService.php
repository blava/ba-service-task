<?php

namespace App\Service;

use App\DTO\TaskDTO;
use App\DTO\UserDTO;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Validation\TaskValidation;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Transport\Serialization\Serializer;

class UserService
{
    private UserRepository $userRepository;
    private TaskRepository $taskRepository;
    private TaskValidation $taskValidation;
    private EventDispatcherInterface $eventDispatcher;
    private GuzzleService $guzzleService;

    public function __construct(
        UserRepository           $userRepository,
        TaskRepository           $taskRepository,
        TaskValidation           $taskValidation,
        EventDispatcherInterface $eventDispatcher,
        GuzzleService $guzzleService
    )
    {
        $this->userRepository = $userRepository;
        $this->taskRepository = $taskRepository;
        $this->taskValidation = $taskValidation;
        $this->eventDispatcher = $eventDispatcher;
        $this->guzzleService = $guzzleService;
    }

    public function getUserDetails(UserDTO $userDTO)
    {
        $user = $this->userRepository->find($userDTO->getId());

        if (empty($user))
            throw new \Exception("User doesn't exist");
    }

    public function manageTaskDetails(Request $request, int $taskId)
    {
        $task = $this->taskRepository->find($taskId);
        if (empty($task))
            throw new \Exception("Task couldn't be found");
//        $user = $this->guzzleService->getUser($task->getUserId());
        $status = json_decode($request->getContent(), true)['status'];

        $this->taskValidation->validate($status);
        $task->setStatus($status);
        $this->taskRepository->save($task, true);

        $task = new TaskDTO($task);
        if ($status == 'complete') {
            $this->eventDispatcher->dispatch($task, TaskDTO::class);
        }

    }
}