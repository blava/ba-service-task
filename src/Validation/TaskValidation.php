<?php

namespace App\Validation;

use App\DTO\TaskDTO;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaskValidation
{
    public function validate(string $status) {
        if (!in_array($status, ['new', 'on-process', 'complete']))
            throw new \Exception("Invalid task status");
    }
}