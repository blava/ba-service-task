<?php

namespace App\DTO;

use App\Entity\Task;

class TaskDTO implements GeneralTaskInterface
{

    const TASK_CHANGED_EVENT = "task.event.changed";
//id, userId, status(new, complete, cancel), title, description) and REST for IT
    private ?int $id;

    private ?int $userId;

    private ?string $status;
    private ?string $title;
    private ?string $description;


     public function __construct(Task $task)
     {
         $this->id = $task->getId();
         $this->userId = $task->getUserId();
         $this->status = $task->getStatus();
     }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

}