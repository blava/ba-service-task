<?php

namespace App\MessageHandler;

use App\Entity\Task;
use App\Message\UserPayload;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class UserPayloadHandler
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function __invoke(UserPayload $payload)
    {

        var_dump("TESSSSSSSSSSSSTT " . $payload);

        $task = new Task();
        $task->setUserId($payload->getId());
        $task->setStatus('new');
        $task->setTitle($payload->getName());
        $task->setDescription('this is a description');

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }
}