<?php

namespace App\MessageHandler;

use App\Entity\Task;
use App\Message\Payload;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class PayloadHandler
{

    public function __invoke(Payload $payload)
    {
//        $task = new Task();
//        $task->setUserId($payload->getId());
//        $task->setStatus('new');
//        $task->setTitle($payload->getName());

        echo json_encode($payload, true);
//        $entityManager->persist($task);
//        $entityManager->flush();
    }
}