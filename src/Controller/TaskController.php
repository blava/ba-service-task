<?php

namespace App\Controller;

use App\DTO\TaskDTO;
use App\DTO\User;
use App\Message\TaskPayload;
use App\Repository\TaskRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TaskController extends AbstractController
{

    private TaskRepository $taskRepository;
    private UserService $userService;
    public function __construct(TaskRepository $taskRepository, UserService $userService )
    {
        $this->taskRepository = $taskRepository;
        $this->userService = $userService;
    }

    #[Route('/task', name: 'app_task')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TaskController.php',
        ]);
    }

    #[Route('/task/{id}', name: 'edit_task', methods: 'PUT')]
    public function update(Request $request, int $id): Response
    {
        $this->userService->manageTaskDetails($request, $id);
//            $guzzle = new \GuzzleHttp\Client();
//            $res= $guzzle->request('GET', "http://localhost:8000/user/" .$task->getUserId());
//            if ($res->getStatusCode() == 404)
//                return new Response('User not found' , 404);
//            $userObject =  $serializer->deserialize($res->getBody(), User::class,'json');
//            if (!$userObject || $userObject->getActive() ==0 )
//                return new Response('User not found' , 404);
//            $repository->save($task, true);
//            if ($task->getStatus() == 'complete') {
//                $messageBus->dispatch(new TaskPayload($task->getId(), $task->getUserId(), $task->getStatus()));
//            }
//        } catch (\Exception $exception) {
//            throw new \Exception($exception->getMessage());
//        }
        return new Response("Task has been updated", 201);
    }

}
