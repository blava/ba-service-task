<?php

namespace App\Event;

use App\DTO\TaskDTO;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class TaskEventSubscriber implements EventSubscriberInterface
{

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskDTO::class => 'onTaskChangedStatusEvent',
        ];
    }

    public function onTaskChangedStatusEvent(TaskDTO $task): void
    {
        $this->messageBus->dispatch($task);
    }
}